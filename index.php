<?php
// set the default timezone to use.
date_default_timezone_set('UTC');

echo date('d/m/Y H:i:s');